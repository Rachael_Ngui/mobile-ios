package stepdef;

import com.application.actions.PayScreenOfOtherEquityAccountActions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class Pay_To_Other_Equity_Account_Using_Mobile_Number {

	@Then("click on send money to Other Equity Account")
	public void click_on_send_money_to_Other_Equity_Account() {
		PayScreenOfOtherEquityAccountActions ps= new PayScreenOfOtherEquityAccountActions();
		ps.sendMoneyToOtherEquity();
	}

	@And("click on select account")
	public void click_on_select_account() {
		PayScreenOfOtherEquityAccountActions ps= new PayScreenOfOtherEquityAccountActions();
		ps.select_Account();
	}

	@Then("user select the some one")
	public void user_select_the_some_one() {
		PayScreenOfOtherEquityAccountActions ps= new PayScreenOfOtherEquityAccountActions();
		ps.someOne();
	}

	@Then("user enters the mobile number {string}")
	public void user_enters_the_acount_number(String number) {
		PayScreenOfOtherEquityAccountActions ps= new PayScreenOfOtherEquityAccountActions();
		ps.mobile_number_field(number);
	}

	@And("click on Add button in Other Equity Account")
	public void click_on_Add_button_in_Other_Equity_Account() {
		PayScreenOfOtherEquityAccountActions ps= new PayScreenOfOtherEquityAccountActions();
		ps.add();
	}
	
	@Then("user enters other Equity amount {string}")
	public void user_enters_other_Equity_amount(String account) {
		PayScreenOfOtherEquityAccountActions ps= new PayScreenOfOtherEquityAccountActions();
		ps.account_field(account);
	}

	@And("click on Add button in Other Equity Account using mobile number")
	public void click_on_Add_button_in_Oher_Equity_Account_using_mobile_number() {
		PayScreenOfOtherEquityAccountActions ps= new PayScreenOfOtherEquityAccountActions();
		ps.add();
	}

	
	@Then("click on send money button of Other Equity Account")
	public void click_on_send_money_button_of_Other_Equity_Account() {
		PayScreenOfOtherEquityAccountActions ps= new PayScreenOfOtherEquityAccountActions();
		ps.send();
	}
	
	@Then("click on Pay button of Other Equity Account")
	public void click_on_Pay_button_of_Other_Equity_Account() {
		PayScreenOfOtherEquityAccountActions ps= new PayScreenOfOtherEquityAccountActions();
		ps.pay();
	}

	@And("click on Done button of Other Equity Account")
	public void click_on_Done_button_of_Other_Equity_Account() {
		PayScreenOfOtherEquityAccountActions ps= new PayScreenOfOtherEquityAccountActions();
		ps.done();
	}
}
