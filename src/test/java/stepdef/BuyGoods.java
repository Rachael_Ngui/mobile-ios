package stepdef;

import com.application.actions.BuyGoodsScreenActions;
import com.application.base.RunnableClass;
import com.application.libraries.Excel;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class BuyGoods extends RunnableClass{

	String account;
	String amount;

	@Before
	public String[] readLoginData() {
		Excel e = new Excel();
		System.out.println("******************** Buy Goods ********************");
		account = e.ReadCellData(2, 2, 12);
		System.out.println("Account number :" + " " + account);
		amount = e.ReadCellData(2, 2, 13);
		System.out.println("Amount :" + " " + amount);
		System.out.println("*******************************************************");
		String data[] = { account, amount };
		return data;
	}
	
	@Given("user naviate to Buy goods")
	public void user_naviate_to_Buy_goods() {
		BuyGoodsScreenActions b = new BuyGoodsScreenActions();
		b.buygoods();
		try {
			stepdef = extent.createTest(Feature.class, "Buy Goods");
			stepdef = stepdef.createNode(Scenario.class, "Checking for Buy Goods");
			logInfo = stepdef.createNode(new GherkinKeyword("Given"), "user naviate to Buy goods");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("select account")
	public void select_account() {
		BuyGoodsScreenActions b = new BuyGoodsScreenActions();
		b.select_account();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "select account");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@When("user pay through another till")
	public void user_pay_through_another_till() {
		BuyGoodsScreenActions b = new BuyGoodsScreenActions();
		b.pay_another_till();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("When"), "user pay through another till");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("user selects country")
	public void user_selects_country() {
		BuyGoodsScreenActions b = new BuyGoodsScreenActions();
		b.country();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user selects country");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("user selects till")
	public void user_selects_till() {
		BuyGoodsScreenActions b = new BuyGoodsScreenActions();
		b.select_till();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user selects till");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("user enters account number of Buy goods")
	public void user_enters_account_number_of_Buy_goods() {
		BuyGoodsScreenActions b = new BuyGoodsScreenActions();
		b.account_number(account);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user enters account number of Buy goods");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("clicks on add")
	public void clicks_on_add() {
		BuyGoodsScreenActions b = new BuyGoodsScreenActions();
		b.add();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "clicks on add");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("user enters bill amount")
	public void user_enters_bill_amount() {
		BuyGoodsScreenActions b = new BuyGoodsScreenActions();
		b.bill_amount(amount);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user enters bill amount");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("user clicks on Continue of Buy goods")
	public void user_clicks_on_Continue_of_Buy_goods() {
		BuyGoodsScreenActions b = new BuyGoodsScreenActions();
		b.Continue();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user clicks on Continue of Buy goods");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("user clicks on pay of Buy goods")
	public void user_clicks_on_pay_of_Buy_goods() {
		BuyGoodsScreenActions b = new BuyGoodsScreenActions();
		b.pay();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "uuser clicks on pay of Buy goods");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("user clicks on done of Buy goods")
	public void user_clicks_on_done_of_Buy_goods() {
		BuyGoodsScreenActions b = new BuyGoodsScreenActions();
		b.done();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user clicks on done of Buy goods");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}
}
