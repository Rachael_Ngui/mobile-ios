package stepdef;

import com.application.actions.SendMoneyToMobileWalletAmountNegativeActions;

import io.cucumber.java.en.Then;

public class SendMoneyTo_MobileWalletAmountNegative {

	@Then("click on send money to someone in Mobile wallet")
	public void click_on_send_money_to_someone_in_Mobile_wallet() {
		SendMoneyToMobileWalletAmountNegativeActions s = new SendMoneyToMobileWalletAmountNegativeActions();
		s.sendMoneyToMobileWallet();
	}

	@Then("click on mobile wallet in Mobile wallet")
	public void click_on_mobile_wallet_in_Mobile_wallet() {
		SendMoneyToMobileWalletAmountNegativeActions s = new SendMoneyToMobileWalletAmountNegativeActions();
		s.mobile_wallet();
	}

	@Then("user select the account in Mobile wallet")
	public void user_select_the_account_in_Mobile_wallet() {
		SendMoneyToMobileWalletAmountNegativeActions s = new SendMoneyToMobileWalletAmountNegativeActions();
		s.select_Account();
	}

	@Then("user choose the respective account which are available in Mobile wallet")
	public void user_choose_the_respective_account_which_are_available_in_Mobile_wallet() {
		SendMoneyToMobileWalletAmountNegativeActions s = new SendMoneyToMobileWalletAmountNegativeActions();
		s.someOne_MobileWallet();
	}

	@Then("user select country in Mobile wallet")
	public void user_select_country_in_Mobile_wallet() {
		SendMoneyToMobileWalletAmountNegativeActions s = new SendMoneyToMobileWalletAmountNegativeActions();
		s.selectCountry();
	}

	@Then("country name in Mobile wallet")
	public void country_name_in_Mobile_wallet() {
		SendMoneyToMobileWalletAmountNegativeActions s = new SendMoneyToMobileWalletAmountNegativeActions();
		s.country();
	}

	@Then("user chooses PESA account in Mobile wallet")
	public void user_chooses_PESA_account_in_Mobile_wallet() {
		SendMoneyToMobileWalletAmountNegativeActions s = new SendMoneyToMobileWalletAmountNegativeActions();
		s.MPESA();
	}

	@Then("user enters the number {string} in Mobile wallet")
	public void user_enters_the_number_in_Mobile_wallet(String number) {
		SendMoneyToMobileWalletAmountNegativeActions s = new SendMoneyToMobileWalletAmountNegativeActions();
		s.number(number);
	}

	@Then("click on Add button in Mobile Wallet")
	public void click_on_Add_button_in_Mobile_Wallet() {

		SendMoneyToMobileWalletAmountNegativeActions s = new SendMoneyToMobileWalletAmountNegativeActions();
		s.add();
	}

	@Then("user enters in mobile wallet amount {string}")
	public void user_enters_in_mobile_wallet_amount(String amount) {
		SendMoneyToMobileWalletAmountNegativeActions s = new SendMoneyToMobileWalletAmountNegativeActions();
		s.mobile_Wallet_amount(amount);
	}
}
