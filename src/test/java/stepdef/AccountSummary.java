package stepdef;

import com.application.actions.AccountSummaryScreenActions;
import com.application.base.RunnableClass;
import com.application.libraries.Excel;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class AccountSummary extends RunnableClass {

	String date;
	String expectedText;
	String expectedAvailableText;
	String expectedTotalText;

	@Before
	public String[] readLoginData() {
		Excel e = new Excel();
		System.out.println("******************** Account summary ********************");
		date = e.ReadCellData(1, 2, 11);
		System.out.println("Choosen Date:" + " " + date);
		expectedText = e.ReadCellData(1, 2, 12);
		System.out.println("Expected Text:" + " " + expectedText);
		expectedAvailableText = e.ReadCellData(1, 2, 13);
		System.out.println("Expected Available balance Text:" + " " + expectedAvailableText);
		expectedTotalText = e.ReadCellData(1, 2, 14);
		System.out.println("Expected Total balance Text:" + " " + expectedTotalText);
		System.out.println("*******************************************************");
		String data[] = { date, expectedText, expectedAvailableText, expectedTotalText};
		return data;
	}

	@Given("click on account name")
	public void clcik_on_account_name() throws InterruptedException {
		AccountSummaryScreenActions a = new AccountSummaryScreenActions();
		try {
			a.list(driver, expectedText, expectedAvailableText, expectedTotalText);
		} catch (Exception e) {
			System.out.println(" Account summary not matching ");
		}
	}

	@Then("navigate back to home screen")
	public void navigate_back_to_home_screen() throws InterruptedException {
		AccountSummaryScreenActions a = new AccountSummaryScreenActions();
		a.filter();
	}

	@And("scroll down to history details")
	public void scroll_down_to_history_details() throws InterruptedException {
		AccountSummaryScreenActions a = new AccountSummaryScreenActions();
		a.history();
	}

}
