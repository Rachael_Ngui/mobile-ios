package stepdef;

import com.application.actions.SendMoneyToOtherEquityAccountAmountNegativeActions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class SendMoneyTo_OtherEquityAccountNegative {

	@Then("click on send money to Other Equity Account in Account number")
	public void click_on_send_money_to_Other_Equity_Account_in_Account_number() {
		SendMoneyToOtherEquityAccountAmountNegativeActions s =new SendMoneyToOtherEquityAccountAmountNegativeActions();
		s.sendMoneyToOtherEquity();
	}

	@And("click on select account in Account number")
	public void click_on_select_account_in_Account_number() {
		SendMoneyToOtherEquityAccountAmountNegativeActions s =new SendMoneyToOtherEquityAccountAmountNegativeActions();
		s.select_Account();
	}

	@Then("user select the some one in Account number")
	public void user_select_the_some_one_in_Account_number() {
		SendMoneyToOtherEquityAccountAmountNegativeActions s =new SendMoneyToOtherEquityAccountAmountNegativeActions();
		s.someOne();
	}

	@Then("user enters the account number {string} in Account number")
	public void user_enters_the_account_number_in_Account_number(String number) {
		SendMoneyToOtherEquityAccountAmountNegativeActions s =new SendMoneyToOtherEquityAccountAmountNegativeActions();
		s.mobile_number_field(number);
	}

	@And("click on Add button in Other Equity Account in Account number")
	public void click_on_Add_button_in_Other_Equity_Account_in_Account_number() {
		SendMoneyToOtherEquityAccountAmountNegativeActions s =new SendMoneyToOtherEquityAccountAmountNegativeActions();
		s.add();
	}
	
	@Then("user enters other Equity amount {string} in Account number")
	public void user_enters_other_Equity_amount_in_Account_number(String amount) {
		SendMoneyToOtherEquityAccountAmountNegativeActions s =new SendMoneyToOtherEquityAccountAmountNegativeActions();
		s.account_field(amount);
	}
}
