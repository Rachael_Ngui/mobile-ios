package stepdef;

import com.application.actions.PayScreenOfOtherBankAccountActions;
import com.application.base.RunnableClass;
import com.application.libraries.Excel;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class SendMoneyTo_OtherBank extends RunnableClass{

	String bank;
	String name;
	String account;
	String mobile;
	String amount;

	@Before
	public String[] readData() {
		Excel e = new Excel();
		bank = e.ReadCellData(0, 3, 7);
		System.out.println("Bank name:" + " " + bank);
		name = e.ReadCellData(0, 2, 8);
		System.out.println("Name:" + " " + name);
		account = e.ReadCellData(0, 2, 9);
		System.out.println("Account:" + " " + account);
		mobile = e.ReadCellData(0, 2, 3);
		System.out.println("Mobile:" + " " + mobile);
		amount = e.ReadCellData(0, 3, 10);
		System.out.println("Amount:" + " " + amount);
		String data[] = { bank, name, account, mobile, amount };
		return data;
	}

	@Given("user will navigate to Other Bank")
	public void user_will_navigate_to_Other_Bank() {
		PayScreenOfOtherBankAccountActions p = new PayScreenOfOtherBankAccountActions();
		p.OtherBankAccount();
		try {
			stepdef = extent.createTest(Feature.class, "Send money to ");
			stepdef = stepdef.createNode(Scenario.class, "Checking various Transactions using PayBills");
			logInfo = stepdef.createNode(new GherkinKeyword("Given"), "user will navigate to Other Bank");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("user select account in other bank")
	public void user_select_account_in_other_bank() {
		PayScreenOfOtherBankAccountActions p = new PayScreenOfOtherBankAccountActions();
		p.select_Account();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user select account in other bank");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("choose some one new in other bank")
	public void choose_some_one_new_in_other_bank() {
		PayScreenOfOtherBankAccountActions p = new PayScreenOfOtherBankAccountActions();
		p.SomeOneNew();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "choose some one new in other bank");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@When("user searches for in other bank")
	public void user_searches_for_in_other_bank() {
		PayScreenOfOtherBankAccountActions p = new PayScreenOfOtherBankAccountActions();
		p.search(bank);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("When"), "user searches for in other bank");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("user enters full name")
	public void user_enters_full_name() {
		PayScreenOfOtherBankAccountActions p = new PayScreenOfOtherBankAccountActions();
		p.fullName(name);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user enters full name");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("user enters account number")
	public void user_enters_account_number() {
		PayScreenOfOtherBankAccountActions p = new PayScreenOfOtherBankAccountActions();
		p.accountNumber(account);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user enters account number");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

//	@And("user enters mobile number")
//	public void user_enters_mobile_number() {
//		PayScreenOfOtherBankAccountActions p = new PayScreenOfOtherBankAccountActions();
//		p.number(mobile);
//		try {
//			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user enters mobile number");
//			logInfo.pass("Pass");
//		} catch (Exception e) {
//			logInfo.info("fail");
//		}
//	}

	@When("clicks on Add")
	public void clicks_on_Add() {
		PayScreenOfOtherBankAccountActions p = new PayScreenOfOtherBankAccountActions();
		p.add();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("When"), "clicks on Add");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("user enters amount in other bank")
	public void user_enters_amount_in_other_bank() {
		PayScreenOfOtherBankAccountActions p = new PayScreenOfOtherBankAccountActions();
		p.amountField(amount);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user enters amount in other bank");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("click on send money button in other bank")
	public void click_on_send_money_button_in_other_bank() {
		PayScreenOfOtherBankAccountActions p = new PayScreenOfOtherBankAccountActions();
		p.send();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "click on send money button in other bank");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("click on Pay button in other bank")
	public void click_on_Pay_button_in_other_bank() {
		PayScreenOfOtherBankAccountActions p = new PayScreenOfOtherBankAccountActions();
		p.pay();try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "click on Pay button in other bank");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("click on Done button in other bank")
	public void click_on_Done_button_in_other_bank() {
		PayScreenOfOtherBankAccountActions p = new PayScreenOfOtherBankAccountActions();
		p.done();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "click on Done button in other bank");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

}
