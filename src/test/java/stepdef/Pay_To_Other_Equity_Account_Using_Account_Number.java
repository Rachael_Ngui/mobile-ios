package stepdef;

import com.application.actions.PayScreenOfOtherEquityAccountMobileActions;
import com.application.base.RunnableClass;
import com.application.libraries.Excel;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class Pay_To_Other_Equity_Account_Using_Account_Number extends RunnableClass{

	String account;
	String amount;
	ExtentTest logInfo = null;
	
	@Before
	public String[] readData() {
		Excel e = new Excel();
		System.out.println("********** Pay to Other Equity Account **********");
		account = e.ReadCellData(3, 2, 5);
		System.out.println("Account number is:" + " " + account);
		amount = e.ReadCellData(3, 2, 6);
		System.out.println("Amount entered is:" + " " + amount);
		System.out.println("*************************************************");
		String data[] = { account , amount};
		return data;
	}
	
	@Then("click on send money to Other Equity Account using Account number")
	public void click_on_send_money_to_Other_Equity_Account_using_Account_number() {
		PayScreenOfOtherEquityAccountMobileActions m= new PayScreenOfOtherEquityAccountMobileActions();
		m.sendMoneyToOtherEquity();
		try {
			stepdef = extent.createTest(Feature.class, "Pay Functionality feature through Other Equity Account");
			stepdef = stepdef.createNode(Scenario.class, "Checking various Transactions using account number for Equity Account");
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "click on send money to Other Equity Account");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.error("fail");
		}
	}

	@And("click on select account using Account number")
	public void click_on_select_account_using_Account_number() {
		PayScreenOfOtherEquityAccountMobileActions m= new PayScreenOfOtherEquityAccountMobileActions();
		m.select_Account();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "click on select account");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.error("fail");
		}

	}

	@Then("user select the some one using Account number")
	public void user_select_the_some_one_using_Account_number() {
		PayScreenOfOtherEquityAccountMobileActions m= new PayScreenOfOtherEquityAccountMobileActions();
		m.someOne();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user select the some one");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.error("fail");
		}
	}

	@Then("user enters the account number using Account number")
	public void user_enters_the_account_number_using_Account_number(){
		PayScreenOfOtherEquityAccountMobileActions m= new PayScreenOfOtherEquityAccountMobileActions();
		m.account_number_field(account);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user enters the account number");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.error("fail");
		}
	}

	@And("click on Add button in Other Equity Account using Account number")
	public void click_on_Add_button_in_Other_Equity_Account_using_Account_number() {
		PayScreenOfOtherEquityAccountMobileActions m= new PayScreenOfOtherEquityAccountMobileActions();
		m.add();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user enters the account number");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.error("fail");
		}
	}

	@Then("user enters other Equity amount using Account number")
	public void user_enters_other_Equity_amount_using_Account_number(){
		PayScreenOfOtherEquityAccountMobileActions m= new PayScreenOfOtherEquityAccountMobileActions();
		m.account_field(amount);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user enters the account number");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.error("fail");
		}
	}

	@And("click on send money button of Other Equity Account using Account number")
	public void click_on_send_money_button_of_Other_Equity_Account_using_Account_number() {
		PayScreenOfOtherEquityAccountMobileActions m= new PayScreenOfOtherEquityAccountMobileActions();
		m.send();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "click on send money button of Other Equity Account");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.error("fail");
		}
	}

	@Then("click on Pay button of Other Equity Account using Account number")
	public void click_on_Pay_button_of_Other_Equity_Account_using_Account_number() {
		PayScreenOfOtherEquityAccountMobileActions m= new PayScreenOfOtherEquityAccountMobileActions();
		m.pay();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "click on Pay button of Other Equity Account");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.error("fail");
		}
	}

	@And("click on Done button of Other Equity Account using Account number")
	public void click_on_Done_button_of_Other_Equity_Account_using_Account_number() {
		PayScreenOfOtherEquityAccountMobileActions m= new PayScreenOfOtherEquityAccountMobileActions();
		m.done();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "click on Done button of Other Equity Account");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.error("fail");
		}
	}
}
