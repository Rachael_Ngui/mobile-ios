package stepdef;

import com.application.actions.LoginScreenAction;
import com.application.base.RunnableClass;
import com.application.libraries.Excel;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Login extends RunnableClass {

	String username;
	String password;

	@Before
	public String[] readLoginData() {
		Excel e = new Excel();
		System.out.println("******************** Login Details ********************");
		username = e.ReadCellData(2, 2, 0);
		System.out.println("Username:" + " " + username);
		password = e.ReadCellData(2, 2, 1);
		System.out.println("Password:" + " " + password);
		System.out.println("*******************************************************");
		String data[] = { username, password };
		return data;
	}

	@Given("user launch Equity app")
	public void user_launch_Equity_app() throws InterruptedException {
		RunnableClass.IOS_launcher();

		LoginScreenAction l = new LoginScreenAction();
		l.language_Select();
		try {
			stepdef = extent.createTest(Feature.class, "Login Functionality Feature");
			stepdef = stepdef.createNode(Scenario.class, "Login to Equity");
			logInfo = stepdef.createNode(new GherkinKeyword("Given"), "user launch Equity app");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@When("user logs in using username")
	public void user_logs_in_using_username_as() {
		LoginScreenAction l = new LoginScreenAction();
		l.username_Field(username);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("When"), "user logs in using username");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}

	}

	@And("password")
	public void password_as() {
		LoginScreenAction l = new LoginScreenAction();
		l.password_Field(password);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "password");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}

	}

	@Then("Home screen should appear")
	public void Home_screen_should_appear() throws InterruptedException {
		LoginScreenAction l = new LoginScreenAction();
		l.click_On_ContinueButton();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "Home screen should appear");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}

	}
}
