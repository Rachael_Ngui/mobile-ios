package stepdef;

import com.application.actions.SendMoneyToMobileWalletNegativeActions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.But;
import io.cucumber.java.en.Then;

public class SendMoneyTo_MobileWalletNegative {

	@Then("click on send money to someone using Mobile wallet")
	public void click_on_send_money_to_someone_using_Mobile_wallet() {
		SendMoneyToMobileWalletNegativeActions s = new SendMoneyToMobileWalletNegativeActions();
		s.sendMoneyToMobileWallet();
	}

	@And("click on mobile wallet using Mobile wallet")
	public void click_on_mobile_wallet_using_Mobile_wallet() {
		SendMoneyToMobileWalletNegativeActions s = new SendMoneyToMobileWalletNegativeActions();
		s.mobile_wallet();
	}

	@Then("user select the account using Mobile wallet")
	public void user_select_the_account_using_Mobile_wallet() {
		SendMoneyToMobileWalletNegativeActions s = new SendMoneyToMobileWalletNegativeActions();
		s.select_Account();
	}

	@But("user choose the respective account which are available using Mobile wallet")
	public void user_choose_the_respective_account_which_are_available_using_Mobile_wallet() {
		SendMoneyToMobileWalletNegativeActions s = new SendMoneyToMobileWalletNegativeActions();
		s.someOne_MobileWallet();
	}

	@Then("user select country using Mobile wallet")
	public void user_select_country_using_Mobile_wallet() {
		SendMoneyToMobileWalletNegativeActions s = new SendMoneyToMobileWalletNegativeActions();
		s.selectCountry();
	}

	@And("country name using Mobile wallet")
	public void country_name_using_Mobile_wallet() {
		SendMoneyToMobileWalletNegativeActions s = new SendMoneyToMobileWalletNegativeActions();
		s.country();
	}

	@And("user chooses PESA account using Mobile wallet")
	public void user_chooses_PESA_account_using_Mobile_wallet() {
		SendMoneyToMobileWalletNegativeActions s = new SendMoneyToMobileWalletNegativeActions();
		s.MPESA();
	}

	@Then("user enters the number {string} using Mobile wallet")
	public void user_enters_the_number_using_Mobile_wallet(String number) {
		SendMoneyToMobileWalletNegativeActions s = new SendMoneyToMobileWalletNegativeActions();
		s.number(number);
	}
}
