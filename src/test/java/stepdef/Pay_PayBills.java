package stepdef;

import com.application.actions.PayScreenOfPayBillsActions;
import com.application.base.RunnableClass;
import com.application.libraries.Excel;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class Pay_PayBills extends RunnableClass{

	String biller_number;
	String account;
	String amount;
	
	@Before
	public String[] readData() {
		Excel e = new Excel();
		System.out.println("********** Pay Bills **********");
		biller_number = e.ReadCellData(2, 2, 9);
		System.out.println("Biller number is:" + " " + biller_number);
		account = e.ReadCellData(2, 2, 10);
		System.out.println("account entered is:" + " " + account);
		amount = e.ReadCellData(2, 2, 11);
		System.out.println("amount entered is:" + " " +amount);
		System.out.println("*************************************************");
		String data[] = { account , account, amount};
		return data;
	}
	
	@Then("user navigate to PayBills")
	public void user_navigate_to_PayBills() {
		PayScreenOfPayBillsActions p = new PayScreenOfPayBillsActions();
		p.payBills();
		try {
			stepdef = extent.createTest(Feature.class, "Pay Bill Feature");
			stepdef = stepdef.createNode(Scenario.class, "Checking various Transactions using PayBills");
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user navigate to PayBills");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("user clicks on selectBiller")
	public void user_clicks_on_selectBiller() {
		PayScreenOfPayBillsActions p = new PayScreenOfPayBillsActions();
		p.select_biller();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user clicks on selectBiller");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("user select utilities")
	public void user_select_utilities() {
		PayScreenOfPayBillsActions p = new PayScreenOfPayBillsActions();
		p.utilities();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user select utilities");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("user searches for")
	public void user_searches_for() {
		PayScreenOfPayBillsActions p = new PayScreenOfPayBillsActions();
		p.search(biller_number);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user searches for");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

//	@And("user select billers")
//	public void user_select_billers() {
//		PayScreenOfPayBillsActions p = new PayScreenOfPayBillsActions();
//		p.rizki();
//	}

//	@Then("user clicks on Next button")
//	public void user_clicks_on_Next_button() {
//		PayScreenOfPayBillsActions p = new PayScreenOfPayBillsActions();
//		p.next();
//		try {
//			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user clicks on Next button");
//			logInfo.pass("Pass");
//		} catch (Exception e) {
//			logInfo.info("fail");
//		}
//	}

	@And("user clicks on account number")
	public void user_clicks_on_account_number() {
		PayScreenOfPayBillsActions p = new PayScreenOfPayBillsActions();
		p.bill_amount(account);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user clicks on account number");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("user clicks on add button")
	public void user_clicks_on_add_button() {
		PayScreenOfPayBillsActions p = new PayScreenOfPayBillsActions();
		p.add();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user clicks on add button");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("user enters amounts")
	public void user_enters_amount() {
		PayScreenOfPayBillsActions p = new PayScreenOfPayBillsActions();
		p.amount(amount);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user enters amounts");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("user clicks on continue")
	public void user_clicks_on_continue() {
		PayScreenOfPayBillsActions p = new PayScreenOfPayBillsActions();
		p.Continue();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user clicks on continue");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("user clicks on pay")
	public void user_clicks_on_pay() {
		PayScreenOfPayBillsActions p = new PayScreenOfPayBillsActions();
		p.pay();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user clicks on pay");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("user clicks on Done")
	public void user_clicks_on_Done() {
		PayScreenOfPayBillsActions p = new PayScreenOfPayBillsActions();
		p.done();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user clicks on Done");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}
}
