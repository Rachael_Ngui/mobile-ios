package stepdef;

import com.application.actions.SendMoneyToOtherEquityAccountMobileNegativeActions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class SendMoneyTo_OtherEquityAccountMobileNegative {

	@Then("click on send money to Other Equity Account in mobile number")
	public void click_on_send_money_to_Other_Equity_Account_in_mobile_number() {
		SendMoneyToOtherEquityAccountMobileNegativeActions s = new SendMoneyToOtherEquityAccountMobileNegativeActions();
		s.sendMoneyToOtherEquity();
	}

	@And("click on select account in mobile number")
	public void click_on_select_account_in_mobile_number() {
		SendMoneyToOtherEquityAccountMobileNegativeActions s = new SendMoneyToOtherEquityAccountMobileNegativeActions();
		s.select_Account();
	}

	@Then("user select the some one in mobile number")
	public void user_select_the_some_one_in_mobile_number() {
		SendMoneyToOtherEquityAccountMobileNegativeActions s = new SendMoneyToOtherEquityAccountMobileNegativeActions();
		s.someOne();
	}

	@Then("user enters the account number {string} in mobile number")
	public void user_enters_the_account_number_in_mobile_number(String number) {
		SendMoneyToOtherEquityAccountMobileNegativeActions s = new SendMoneyToOtherEquityAccountMobileNegativeActions();
		s.mobile_number_field(number);
	}

	@And("click on add")
	public void click_on_add() {
		SendMoneyToOtherEquityAccountMobileNegativeActions s = new SendMoneyToOtherEquityAccountMobileNegativeActions();
		s.add();
	}
}
