package stepdef;

import com.application.actions.SendToOwnEquityAccountNegativeActions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class SendTo_OwnEquityAccount {

	@Given("navigate to Transact menu for Own Equity Account")
	public void navigate_to_Transact_menu_for_Own_Equity_Account() {
		SendToOwnEquityAccountNegativeActions s = new SendToOwnEquityAccountNegativeActions();
		s.transact();
	}

	@Then("click on send money to yourself for Own Equity Account")
	public void click_on_send_money_to_yourself_for_Own_Equity_Account() {
		SendToOwnEquityAccountNegativeActions s = new SendToOwnEquityAccountNegativeActions();
		try {
			s.sendMoneyToYourself();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@And("user enter amount as {string} for Own Equity Account")
	public void user_enter_amount_as_for_Own_Equity_Account(String amount) {
		SendToOwnEquityAccountNegativeActions s = new SendToOwnEquityAccountNegativeActions();
		s.amount(amount);
	}
}
