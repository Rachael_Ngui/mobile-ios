package stepdef;

import com.application.actions.PayLoanOfAnotherAmountScreenActions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class Pay_Loan {

	@Given("navigate to Pay Loan")
	public void navigate_to_Pay_Loan() {
		PayLoanOfAnotherAmountScreenActions p=new PayLoanOfAnotherAmountScreenActions();
		p.payLoan();
	}

	@And("the select the loan source to pay")
	public void the_select_the_loan_source_to_pay() {
		PayLoanOfAnotherAmountScreenActions p=new PayLoanOfAnotherAmountScreenActions();
		p.selectLoanPay();
	}

	@And("choose the first option")
	public void choose_the_first_option() {
		PayLoanOfAnotherAmountScreenActions p=new PayLoanOfAnotherAmountScreenActions();
		p.option();
	}

	@Then("choose the amount to pay")
	public void choose_the_amount_to_pay() {
		PayLoanOfAnotherAmountScreenActions p=new PayLoanOfAnotherAmountScreenActions();
		p.amount();
	}

	@Then("choose amother amount")
	public void choose_amother_amount() {
		PayLoanOfAnotherAmountScreenActions p=new PayLoanOfAnotherAmountScreenActions();
		p.anotherAmount();
	}

	@And("enter the amount to pay {string}")
	public void enter_the_amount_to_pay(String amount) {
		PayLoanOfAnotherAmountScreenActions p=new PayLoanOfAnotherAmountScreenActions();
		p.anotherAmountEnter(amount);
	}

	@And("click on save button")
	public void click_on_save_button() {
		PayLoanOfAnotherAmountScreenActions p=new PayLoanOfAnotherAmountScreenActions();
		p.save();
	}

	@Then("click on continue button")
	public void click_on_continue_button() {
		PayLoanOfAnotherAmountScreenActions p=new PayLoanOfAnotherAmountScreenActions();
		p.Continue();
	}

	@And("finaly make the payment")
	public void finalyy_make_the_payment() {
		PayLoanOfAnotherAmountScreenActions p=new PayLoanOfAnotherAmountScreenActions();
		p.payment();
	}
}
