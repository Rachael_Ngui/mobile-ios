package stepdef;

import com.application.actions.LoginNegativeScreenActions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginNegative {

	@When("user clicks Continue button")
	public void user_clicks_Continue_button() {
		LoginNegativeScreenActions l=new LoginNegativeScreenActions();
		l.Continue();
		l.Continue();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Then("user gets login failed error message")
	public void user_gets_login_failed_error_message() {
	    System.out.println("Login failed");
	}
}
