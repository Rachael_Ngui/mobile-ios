package stepdef;

import com.application.actions.PayScreenOfMobileWalletActions;
import com.application.base.RunnableClass;
import com.application.libraries.Excel;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.But;
import io.cucumber.java.en.Then;

public class Pay_through_Mobile_Wallet extends RunnableClass {

	String number;
	String amount;

	@Before
	public String[] readData() {
		Excel e = new Excel();
		System.out.println("******************** Mobile Wallet ********************");
		number = e.ReadCellData(3, 2, 3);
		System.out.println("Mobile number is:" + " " + number);
		amount = e.ReadCellData(3, 2, 4);
		System.out.println("Amount entered is:" + " " + amount);
		System.out.println("*******************************************************");
		String data[] = { number, amount };
		return data;
	}

	@Then("click on send money to someone")
	public void click_on_send_money_to_someone() {
		PayScreenOfMobileWalletActions ps = new PayScreenOfMobileWalletActions();
		ps.sendMoneyToMobileWallet();
		try {
			stepdef = extent.createTest(Feature.class, "Pay Functionality feature through Mobile wallet");
			stepdef = stepdef.createNode(Scenario.class, "Checking various Transactions using Mobile wallet");
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "click on send money to someone");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("click on mobile wallet")
	public void click_on_mobile_wallet() {
		PayScreenOfMobileWalletActions ps = new PayScreenOfMobileWalletActions();
		ps.mobile_wallet();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "click on mobile wallet");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("user select the account")
	public void user_select_the_account() {
		PayScreenOfMobileWalletActions ps = new PayScreenOfMobileWalletActions();
		ps.select_Account();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user select the account");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@But("user choose the respective account which are available")
	public void user_choose_the_respective_account_which_are_available() {
		PayScreenOfMobileWalletActions ps = new PayScreenOfMobileWalletActions();
		ps.someOne_MobileWallet();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("But"), "user choose the respective account which are available");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("select operator")
	public void user_operator() {
		PayScreenOfMobileWalletActions ps = new PayScreenOfMobileWalletActions();
		ps.operator();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "select operator");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

//	@Then("user select country")
//	public void user_select_country() {
//		try {
//			PayScreenOfMobileWalletActions ps = new PayScreenOfMobileWalletActions();
//			ps.selectCountry();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@And("country name")
//	public void country_name() {
//		try {
//			PayScreenOfMobileWalletActions ps = new PayScreenOfMobileWalletActions();
//			ps.country();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@And("user chooses PESA account")
//	public void user_chooses_PESA_account() {
//		try {
//			PayScreenOfMobileWalletActions ps = new PayScreenOfMobileWalletActions();
//			ps.MPESA();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	@Then("user enters the number")
	public void user_enters_the_number() {
		PayScreenOfMobileWalletActions ps = new PayScreenOfMobileWalletActions();
		ps.number(number);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user enters the number");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("click on Add button")
	public void click_on_Add_button() {
		PayScreenOfMobileWalletActions ps = new PayScreenOfMobileWalletActions();
		ps.add();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "click on Add button");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("user enters mobile wallet amount")
	public void user_enters_mobile_wallet_amount() {
		PayScreenOfMobileWalletActions ps = new PayScreenOfMobileWalletActions();
		ps.mobile_Wallet_amount(amount);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user enters mobile wallet amount");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("click on send money button of mobile wallet")
	public void click_on_send_money_button_of_mobileWallet() {
		PayScreenOfMobileWalletActions ps = new PayScreenOfMobileWalletActions();
		ps.send_Money();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "click on send money button of mobile wallet");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("click on Pay button of mobile wallet")
	public void click_on_Pay_button_of_mobileWallet() {
		PayScreenOfMobileWalletActions ps = new PayScreenOfMobileWalletActions();
		ps.Pay();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "click on Pay button of mobile wallet");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("click on Done button of mobile wallet")
	public void click_on_Done_button_of_mobileWallet() {
		PayScreenOfMobileWalletActions ps = new PayScreenOfMobileWalletActions();
		ps.Done();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "click on Done button of mobile wallet");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}
}
