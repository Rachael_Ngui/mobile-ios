package stepdef;

import com.application.actions.PayScreenOfSendMoneyToYourselfActions;
import com.application.base.RunnableClass;
import com.application.libraries.Excel;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class Pay_To_Yorself extends RunnableClass{

	String amount;
	@Before
	public String[] readData()
	{
		Excel e = new Excel();
		System.out.println("******************** Pay to yourself ********************");
		amount = e.ReadCellData(0, 2, 2);
		System.out.println("Amount entered is:" + " " +amount);
		System.out.println("*********************************************************");
		String data[]= {amount};
		return data;
	}

	@Given("navigate to Transact menu")
	public void navigate_to_Transact_menu() {
		PayScreenOfSendMoneyToYourselfActions ps = new PayScreenOfSendMoneyToYourselfActions();
		ps.transact();
		try {
			stepdef = extent.createTest(Feature.class, "Pay Functionality feature");
			stepdef = stepdef.createNode(Scenario.class, "Checking various Transactions using Send money to Yourself");
			logInfo = stepdef.createNode(new GherkinKeyword("Given"), "navigate to Transact menu");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("click on send money to yourself")
	public void click_on_send_money_to_yourself() {
		PayScreenOfSendMoneyToYourselfActions ps = new PayScreenOfSendMoneyToYourselfActions();
		try {
			ps.sendMoneyToYourself();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "click on send money to yourself");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("user enter amount")
	public void user_enter_amount(){
		PayScreenOfSendMoneyToYourselfActions ps = new PayScreenOfSendMoneyToYourselfActions();
		ps.amount(amount);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "user enter amount");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("click on send money button")
	public void click_on_send_money_button() {
		PayScreenOfSendMoneyToYourselfActions ps = new PayScreenOfSendMoneyToYourselfActions();
		ps.sendMoneyButton();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "click on send money button");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("click on Pay button")
	public void click_on_Pay_button() {
		PayScreenOfSendMoneyToYourselfActions ps = new PayScreenOfSendMoneyToYourselfActions();
		ps.Pay();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "click on Pay button");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("click on Done button")
	public void click_on_Done_button() {
		PayScreenOfSendMoneyToYourselfActions ps = new PayScreenOfSendMoneyToYourselfActions();
		ps.Done();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "click on Done button");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}
}