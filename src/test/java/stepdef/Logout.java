package stepdef;


import com.application.actions.LogoutScreenAction;
import com.application.base.RunnableClass;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Logout extends RunnableClass{

	@When("navigate to More in menu option")
	public void navigate_to_More_in_menu_option() {

		LogoutScreenAction log = new LogoutScreenAction();
		log.click_On_More();
		try {
			stepdef = extent.createTest(Feature.class, "Logout Functionality feature");
			stepdef = stepdef.createNode(Scenario.class, "Logout from Equity");
			logInfo = stepdef.createNode(new GherkinKeyword("When"), "navigate to More in menu option");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("locate the logout option by scrolling down")
	public void locate_the_logout_option_by_scrolling_down() {
		LogoutScreenAction log = new LogoutScreenAction();
		log.scroll_To_Logout();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "locate the logout option by scrolling down");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("logout from application by clicking")
	public void logout_from_application_by_clicking() {
		LogoutScreenAction log = new LogoutScreenAction();
		log.click_On_Logout();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "logout from application by clicking");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}
}
