package stepdef;

import com.application.actions.PayScreenOfBuyAirtimeActions;
import com.application.base.RunnableClass;
import com.application.libraries.Excel;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class Pay_Buyairtime extends RunnableClass{
	
	String Account_Number;
	String amount;
	
	@Before
	public String[] readLoginData() {
		Excel e = new Excel();
		System.out.println("******************** Buy Airtime ********************");
		Account_Number = e.ReadCellData(2, 2, 7);
		System.out.println("Operator number:" + " " + Account_Number);
		amount = e.ReadCellData(2, 2, 8);
		System.out.println("Amount:" + " " + amount);
		System.out.println("*****************************************************");
		String data[] = { Account_Number, amount };
		return data;
		
	}

	@And("scroll to BuyAirtime")
	public void scroll_to_BurAirtime() {
		PayScreenOfBuyAirtimeActions p=new PayScreenOfBuyAirtimeActions();
		p.buyAirTime();
		try {
			stepdef = extent.createTest(Feature.class, "BuyAirtime feature");
			stepdef = stepdef.createNode(Scenario.class, " Checking various Transactions using BuyAirtime");
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "scroll to BuyAirtime");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("user select account")
	public void user_select_account() {
		PayScreenOfBuyAirtimeActions p=new PayScreenOfBuyAirtimeActions();
		p.select_Account();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user select account");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("choose some one new")
	public void choose_some_one_new() {
		PayScreenOfBuyAirtimeActions p=new PayScreenOfBuyAirtimeActions();
		p.someOneNew();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "choose some one new");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("choose")
	public void choose_Equitel() {
		PayScreenOfBuyAirtimeActions p=new PayScreenOfBuyAirtimeActions();
		p.RadioButton();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "choose");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("user enters number")
	public void user_enters_number(){
		PayScreenOfBuyAirtimeActions p=new PayScreenOfBuyAirtimeActions();
		p.number(Account_Number);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user enters number");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("click on Add")
	public void click_on_Add() {
		PayScreenOfBuyAirtimeActions p=new PayScreenOfBuyAirtimeActions();
		p.add();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "click on Add");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("user enters amount")
	public void user_enters_amount() {
		PayScreenOfBuyAirtimeActions p=new PayScreenOfBuyAirtimeActions();
		p.amount(amount);
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("Then"), "user enters amount");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@And("click on Send money")
	public void click_on_Send_money() {
		PayScreenOfBuyAirtimeActions p=new PayScreenOfBuyAirtimeActions();
		p.buy_airtime();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("And"), "click on Send money");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}

	@Then("click on Done in Confirmation screen")
	public void click_on_Done_in_Confirmation_screen() {
		PayScreenOfBuyAirtimeActions p=new PayScreenOfBuyAirtimeActions();
		p.done();
		try {
			logInfo = stepdef.createNode(new GherkinKeyword("When"), "click on Done in Confirmation screen");
			logInfo.pass("Pass");
		} catch (Exception e) {
			logInfo.info("fail");
		}
	}
}
