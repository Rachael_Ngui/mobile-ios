#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: Pay Functionality feature
  I want to use this template for Pay Transactions
  
  
 @positiveScenario
		Scenario: Login to Equity
    Given user launch Equity app
    When user logs in using username as "254763346690"
    And password as "Pass123!"
 		Then Home screen should appear 
 		
  @negativeScenario
  	Scenario Outline: Checking various Transactions 
  	Then click on send money to someone using Mobile wallet
  	And click on mobile wallet using Mobile wallet
  	Then user select the account using Mobile wallet
  	But user choose the respective account which are available using Mobile wallet
  	Then user select country using Mobile wallet
  	And country name using Mobile wallet
  	And user chooses PESA account using Mobile wallet
  	Then user enters the number "<Number>" using Mobile wallet
  	
  	Examples:
  	|Number								|
    |123				  				|			 
  	|12345678901	 				|		
    |aabcd1234 						|			
    |563678  							|			
	
    @negativeScenario
  	Scenario Outline: Checking various Transactions 
  	Then click on send money to someone in Mobile wallet
  	And click on mobile wallet in Mobile wallet
  	Then user select the account in Mobile wallet
  	But user choose the respective account which are available in Mobile wallet
  	Then user select country in Mobile wallet
  	And country name in Mobile wallet
  	And user chooses PESA account in Mobile wallet
  	Then user enters the number "722000000" in Mobile wallet
  	And click on Add button in Mobile Wallet
  	Then user enters in mobile wallet amount "<Amount>"
  	
  	  Examples: 
      | Amount  		| 
      | 123.345 		|  
      | 1234ab$%2 	|
      |	666666 			|
      | 0						|
      |174086.06		|	 
      
  @postivieScenario
  	Scenario: Logout from Equity
    When navigate to More in menu option
    Then locate the logout option by scrolling down
    And logout from application by clicking
  	
  		 
  	
  	