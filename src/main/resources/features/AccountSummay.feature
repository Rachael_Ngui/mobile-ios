#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

		Scenario: Login to Equity
    Given user launch Equity app
    When user logs in using username
    And password
 		Then Home screen should appear  
 		
  	@tag1
  	Scenario: Cross verifying Account Summary
    Given click on account name  
    Then navigate back to home screen
    And  scroll down to history details
    
    @postivieScenario
  	Scenario: Logout from Equity
    When navigate to More in menu option
    Then locate the logout option by scrolling down
    And logout from application by clicking	