#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@UATSendMoneyToOtherBank
Feature: Title of your feature
  I want to use this template for my feature file

Scenario: Login to Equity
    Given user launch Equity app
    When user logs in using username
    And password
 		Then Home screen should appear 
 		
  @tag1
  Scenario: Title of Send money to other bank
    Given user will navigate to Other Bank
    And user select account in other bank
    And choose some one new in other bank
    When user searches for in other bank
    And user enters full name 
    And user enters account number
   # And user enters mobile number
    When click on Add
    And user enters amount in other bank
    And click on send money button in other bank
  	Then click on Pay button in other bank
  	And click on Done button in other bank

