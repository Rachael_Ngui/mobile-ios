#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: Pay Functionality feature
  I want to use this template for Pay Transactions
  
   @positiveScenario
		Scenario: Login to Equity
    Given user launch Equity app
    When user logs in using username as "254763346690"
    And password as "Pass123!"
 		Then Home screen should appear 

  @positiveScenario 
  	Scenario Outline: Checking various Transactions in account number for Equity Account
  	Then click on send money to Other Equity Account in Account number
  	And click on select account in Account number
  	Then user select the some one in Account number
  	Then user enters the account number "1180163966239" in Account number
  	And click on Add button in Other Equity Account in Account number
  	Then user enters other Equity amount "<amount>" in Account number
 
  	
  	Examples:
  	  | amount  	| 
      | 123.345 	|  
      | 1234ab$% 	|
      |	666666 		|
      |174086.06	| 
      
       @postivieScenario
  Scenario: Logout from Equity
    When navigate to More in menu option
    Then locate the logout option by scrolling down
    And logout from application by clicking

