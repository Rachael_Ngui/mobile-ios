package com.application.base;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class RunnableClass {

	public static IOSDriver<WebElement> driver;
	public static DesiredCapabilities cap;
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest stepdef;
	public static ExtentTest features;
	public static ExtentTest logInfo = null;


	public static IOSDriver<WebElement> IOS_launcher() {
		try {
			cap = new DesiredCapabilities();

			cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
			cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12.4.8");
			cap.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 6");
			cap.setCapability(MobileCapabilityType.UDID, "b0b45a64c1236f6c1f87613638ac76f0fc0a2c4f");
			cap.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "ke.co.equitybank.oneequity");
			cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
			cap.setCapability("useNewWDA", true);
			cap.setCapability("noReset", true);
			cap.setCapability("xcodeOrgId", "4LBV99LAYF");
			cap.setCapability("xcodeSigningId", "iPhone Developer");

			driver = new IOSDriver<WebElement>(new URL("http://0.0.0.0:4723/wd/hub"), cap);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return driver;
	}

	public void tearDown() {
		driver.quit();
	}

}