package com.application.base;

import io.appium.java_client.ios.IOSDriver;

public class IOSExtendedDriver {

	public IOSDriver<?> driver;

	private static IOSExtendedDriver gloVar;

	public static IOSExtendedDriver getInstance() {
		if (gloVar == null) {
			gloVar = new IOSExtendedDriver();
		}
		return gloVar;
	}

	/**
	 * @return the driver
	 */
	public final IOSDriver<?> getDriver() {
		return driver;
	}

	/**
	 * @param driver  the driver to set
	 */
	public final void setDriver(IOSDriver<?> driver) {
		this.driver = driver;
	}

}
