package com.application.libraries;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class SwipeActions {

	public static IOSDriver<?> driver;
	
	public static boolean swipeDownwards(IOSDriver<?> driver, String text)
	{
		try {
			Dimension dim=driver.manage().window().getSize();
			int x=dim.getWidth()/2;
			int startY = 0;
			int endY = 0;
			
			switch(text)
			{
			case "up":
				startY=(int)(dim.getHeight() * 0.8);
				endY=(int)(dim.getHeight() * 0.2);
				break;
			case "down":
				startY=(int)(dim.getHeight() * 0.2);
				endY=(int)(dim.getHeight() * 0.8);
				break;
			}
			
			@SuppressWarnings("rawtypes")
			TouchAction t=new TouchAction(driver);
			t.press(PointOption.point(x, startY)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000)))
			.moveTo(PointOption.point(x, endY)).release().perform();
		} catch (Exception e) {
			e.printStackTrace();	
		}
		return true;
		
	} 
	
public static boolean selectSwipeDownToMoveDownwards(IOSDriver<?> driver, String element, int pixelsToSwipe) {
		
		try {
			Point value = null;
			value = driver.findElement(By.xpath(element)).getLocation();
			
			 int x = value.x;
			 int y = value.y;
			 int y1 = value.y+pixelsToSwipe;

			 swipe(driver, x, y1, x, y);
			 return true;
		} catch (Exception e) {
			return true;
		}
	}

public static void swipe(IOSDriver<?> driver, int fromX, int fromY, int toX, int toY) {
	 
	 @SuppressWarnings("rawtypes")
	TouchAction action = new TouchAction(driver);
	 action.press(PointOption.point(fromX,fromY))
	 .waitAction(new WaitOptions().withDuration(Duration.ofMillis(3000))) //you can change wait durations as per your requirement
	 .moveTo(PointOption.point(toX, toY))
	 .release()
	 .perform();
	 }

@SuppressWarnings("rawtypes")
public static void scrollDownIos(IOSDriver<?> driver, double scrollPercentageStart, double scrollPercentageEnd) 
{
    Dimension size = driver.manage().window().getSize();
    System.out.println(size);
    int x = size.getWidth()/2;
    int starty = (int) (size.getHeight() * scrollPercentageStart);
    int endy = (int) (size.getHeight() * scrollPercentageEnd);
    (new TouchAction(driver)).press(PointOption.point(x, starty))
    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
    .moveTo(PointOption.point(x, endy))
    .release()
    .perform();
}

@SuppressWarnings("rawtypes")
public static void scrollDown()
{
	Dimension size = driver.manage().window().getSize();
	int scrollStart= (int) (size.getHeight() * 0.5);
	int scrollEnd= (int) (size.getHeight() * 0.2);
	
	
	new TouchAction((PerformsTouchActions) driver)
	.press(PointOption.point(0, scrollStart))
	.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
	.moveTo(PointOption.point(0, scrollEnd)).release().perform();
}

public static void scroll(WebElement el)
	{ 
	int retry = 0;
		while(retry <=5)
		{
			try
			{
			el.click();
			}catch(Exception e)
			{
				scrollDown();
				retry++;
			}
		}
	}
}
		




