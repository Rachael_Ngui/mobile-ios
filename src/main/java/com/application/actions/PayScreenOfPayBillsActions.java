package com.application.actions;

import com.application.base.RunnableClass;
import com.application.pages.PayScreenOfPayBills;

public class PayScreenOfPayBillsActions extends RunnableClass {

	PayScreenOfPayBills p = new PayScreenOfPayBills(driver);

	public void payBills() {
		p.navigateToTransact.click();
		p.navigateToPayBills.click();
	}

	public void select_biller() {
		p.navigateToSelectBiller.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void utilities() {
		p.navigateToUtilities.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void search(String search) {
		p.navigateToSearch.sendKeys(search);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		p.navigateTo300101.click();
	}

//	public void rizki() {
//		p.navigateToRizikiInsurance.click();
//	}

//	public void next() {
//		p.navigateToNextButton.click();
//	}

	public void bill_amount(String amount) {
		p.navigateToPayBillAccountnumber.sendKeys(amount);
	}

	public void add() {
		p.navigateToAdd.click();
	}

	public void amount(String amount) {
		p.navigateToPayBillAmount.sendKeys(amount);
	}

	public void Continue() {
		p.navigateToContinueButton.click();
	}

	public void pay() {
		p.navigateToPayButton.click();
	}

	public void done() {
		p.navigateToDoneButton.click();
	}
}
