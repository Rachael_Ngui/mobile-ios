package com.application.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.application.base.RunnableClass;
import com.application.libraries.SwipeActions;
import com.application.pages.AccountSummaryScreen;

import io.appium.java_client.ios.IOSDriver;

public class AccountSummaryScreenActions extends RunnableClass {

	AccountSummaryScreen s = new AccountSummaryScreen(driver);

	public void list(IOSDriver<WebElement> driver, String expectedText, String expectedAvailText,
			String expectedTotalText) throws InterruptedException {

		Thread.sleep(5000);

		SwipeActions.swipeDownwards(driver, "up");

		Thread.sleep(2000);

		try {
			s.navigateToAccountListAndClick.click();
		} catch (Exception e) {
			System.out.println("*************** 14941900.0 TZS balance not present ************");
		}
		
		try {
			s.navigateToLuate.click();
		} catch (Exception e) {
			System.out.println("*************** Luate account not present ************");
		}

		try {
			s.navigateToMugumeALex.click();
		} catch (Exception e) {
			System.out.println("*************** Mugume account not present *************");
		}

		try {
			s.navigateToMuyanja.click();
		} catch (Exception e) {
			System.out.println("*************** Muyanja account not present *************");
		}
		Thread.sleep(5000);

		s.navigateToBalance.click();

		Thread.sleep(5000);

		// South sudan
		try {
			WebElement SudanAvailableBalance = driver.findElement(By.name("85722.44"));
			WebElement SudanTotalBalance = driver.findElement(By.name("85722.44"));

			String actual = SudanAvailableBalance.getText();
			if (actual.equals(expectedAvailText))
				System.out.println("************************************");
			System.out.println("Available Balance:" + " " + SudanAvailableBalance.getText());
			System.out.println("****************************************");

			String actual1 = SudanTotalBalance.getText();
			if (actual1.equals(expectedTotalText))
				System.out.println("************************************");
			System.out.println("Total Balance:" + " " + SudanTotalBalance.getText());
			System.out.println("****************************************");

			Thread.sleep(5000);

			driver.navigate().back();

		} catch (Exception e) {
			System.out.println("South Sudan account information is not present to verify");
		}
		// Tanzania
		try {
			WebElement TanzaniaAvailableBalance = driver.findElement(By.name("14941900.00"));
			WebElement TanzaniaTotalBalance = driver.findElement(By.name("14941900.00"));

			String actual = TanzaniaAvailableBalance.getText();
			if (actual.equals(expectedAvailText))
				System.out.println("************************************");
			System.out.println("Available Balance:" + " " + TanzaniaAvailableBalance.getText());
			System.out.println("****************************************");

			String actual1 = TanzaniaTotalBalance.getText();
			if (actual1.equals(expectedTotalText))
				System.out.println("************************************");
			System.out.println("Total Balance:" + " " + TanzaniaTotalBalance.getText());
			System.out.println("****************************************");

			Thread.sleep(5000);

			driver.navigate().back();

		} catch (Exception e) {
			System.out.println("Tanzania account information is not present to verify");
		}

		// Rwanda
		try {
			WebElement MugumeAvailableBalance = driver.findElement(By.name("873217.15"));
			WebElement MugumeTotalBalance = driver.findElement(By.name("873217.15"));

			String actual = MugumeAvailableBalance.getText();
			if (actual.equals(expectedAvailText))
				System.out.println("************************************");
			System.out.println("Mugume Available Balance:" + " " + MugumeAvailableBalance.getText());
			System.out.println("****************************************");

			String actual1 = MugumeTotalBalance.getText();
			if (actual1.equals(expectedTotalText))
				System.out.println("************************************");
			System.out.println("Mugume Total Balance:" + " " + MugumeTotalBalance.getText());
			System.out.println("****************************************");

			Thread.sleep(5000);

			driver.navigate().back();

		} catch (Exception e) {
			System.out.println("Rwanda account information is not present to verify");
		}

		// Uganda
		try {
			WebElement MuyanjaAvailableBalance = driver.findElement(By.name("583375.30"));
			WebElement MuyanjaTotalBalance = driver.findElement(By.name("583375.30"));

			String actual = MuyanjaAvailableBalance.getText();
			if (actual.equals(expectedAvailText))
				System.out.println("************************************");
			System.out.println("Muyanja Available Balance:" + " " + MuyanjaAvailableBalance.getText());
			System.out.println("****************************************");

			String actual1 = MuyanjaTotalBalance.getText();
			if (actual1.equals(expectedTotalText))
				System.out.println("************************************");
			System.out.println("Muyanja Total Balance:" + " " + MuyanjaTotalBalance.getText());
			System.out.println("****************************************");

			Thread.sleep(5000);

			driver.navigate().back();

		} catch (Exception e) {
			System.out.println("Uganda account information is not present to verify");
		}

		// South sudan
		try {
			WebElement SouthSudanBalanceElement = driver.findElement(By.name("85722.44 SSP"));
			WebElement SouthSudanAccountElement = driver
					.findElement(By.name("Acc number 2003111152716 • Savings account"));
			String actualText = SouthSudanAccountElement.getText() + " " + SouthSudanBalanceElement.getText();
			if (actualText.equals(expectedText)) {
				System.out.println("*********************************************************************************");
				System.out.println("Account number:" + SouthSudanAccountElement.getText() + " " + "with Balance is:"
						+ " " + SouthSudanBalanceElement.getText());
				System.out
						.println("**********************************************************************************");
			}
		} catch (Exception e) {
			System.out.println("SouthSudan account details Not matching");
		}

		// Tanzania
		try {
			WebElement TanzaniaBalanceElement = driver.findElement(By.name("14941900.00 TZS"));
			WebElement TanzaniaAccountElement = driver
					.findElement(By.name("Acc number 3004111631457 • Savings account"));

			String actualText = TanzaniaAccountElement.getText() + " " + TanzaniaBalanceElement.getText();
			if (actualText.equals(expectedText)) {
				System.out.println("*********************************************************************************");
				System.out.println("Account number:" + TanzaniaAccountElement.getText() + " " + "with Balance is:" + " "
						+ TanzaniaBalanceElement.getText());
				System.out
						.println("**********************************************************************************");
			}
		} catch (Exception e) {
			System.out.println("Tanzania account details Not matching");
		}

		// Rwanda
		try {
			WebElement MugumeBalanceElement = driver.findElement(By.name("873217.15 RWF"));
			WebElement MugumeAccountElement = driver.findElement(By.name("Acc number 4004200043546 • Savings account"));

			String actualText = MugumeAccountElement.getText() + " " + MugumeBalanceElement.getText();
			if (actualText.equals(expectedText)) {
				System.out.println("*********************************************************************************");
				System.out.println("Account number:" + MugumeAccountElement.getText() + " " + "with Balance is:" + " "
						+ MugumeBalanceElement.getText());
				System.out
						.println("**********************************************************************************");
			}
		} catch (Exception e) {
			System.out.println("Rwanda account details Not matching");
		}

		// Uganda
		try {
			WebElement MuyanjaBalanceElement = driver.findElement(By.name("583375.30 UGX"));
			WebElement MuyanjaAccountElement = driver
					.findElement(By.name("Acc number 1005100419788 • Savings account"));
			String actualText = MuyanjaAccountElement.getText() + " " + MuyanjaBalanceElement.getText();
			if (actualText.equals(expectedText)) {
				System.out.println("*********************************************************************************");
				System.out.println("Account number:" + MuyanjaAccountElement.getText() + " " + "with Balance is:" + " "
						+ MuyanjaBalanceElement.getText());
				System.out
						.println("**********************************************************************************");
			}
		} catch (Exception e) {
			System.out.println("Uganda account details Not matching");
		}
	}

	public void filter() throws InterruptedException {

		Thread.sleep(4000);

		s.navigateToSearchFilter.click();
		s.navigateToFilter.click();
		s.navigateTo1Year.click();
//		s.navigateToFromDate.clear();
//		s.navigateToFromDate.sendKeys(From);
		// s.navigateToToDate.sendKeys(To);
		s.navigateToApplyFilter.click();

		Thread.sleep(5000);

		SwipeActions.swipeDownwards(driver, "up");
		SwipeActions.swipeDownwards(driver, "up");

		Thread.sleep(5000);

		driver.navigate().back();
	}

	public void history() throws InterruptedException {
		SwipeActions.swipeDownwards(driver, "up");
		SwipeActions.swipeDownwards(driver, "up");
		SwipeActions.swipeDownwards(driver, "down");
		SwipeActions.swipeDownwards(driver, "down");

		Thread.sleep(2000);

		driver.navigate().back();
	}

}
