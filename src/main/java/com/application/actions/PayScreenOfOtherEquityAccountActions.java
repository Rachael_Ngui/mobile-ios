package com.application.actions;

import com.application.base.RunnableClass;
import com.application.pages.SendMoneyToOtherEquityAccount;

public class PayScreenOfOtherEquityAccountActions extends RunnableClass{

	SendMoneyToOtherEquityAccount e= new SendMoneyToOtherEquityAccount(driver);
	
	public void sendMoneyToOtherEquity() {
		e.navigateToTransact.click();
		e.navigateToSendMoneyToSomeOne.click();
		e.navigateEquityAccount.click();
	}

	public void select_Account() {
		e.navigateToSelectAccount.click();
	}

	public void someOne() {
		e.navigateToSomeOnenew.click();
	}

	public void mobile_number_field(String number) {
		e.navigateToOtherEquityAccountNumberField.sendKeys(number);
	}

	public void add() {
		e.navigateToAdd.click();
	}

	public void account_field(String amount) {
		e.navigateToOtherEqyityAmountTextField.sendKeys(amount);
	}
	
	public void send()
	{
		e.navigateToSendMoneyButton.click();
	}
	
	public void pay()
	{
		e.navigateToPayButton.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void done()
	{
		e.navigateToDoneButton.click();
		driver.navigate().back();
		driver.navigate().back();
	}
}
