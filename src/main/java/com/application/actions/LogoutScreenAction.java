package com.application.actions;


import com.application.base.RunnableClass;
import com.application.libraries.SwipeActions;
import com.application.pages.LogoutScreen;

public class LogoutScreenAction extends RunnableClass{

	LogoutScreen logout=new LogoutScreen(driver);
	
	public void click_On_More()
	{
		logout.navigateToMore.click();
	}
	
	public void scroll_To_Logout()
	{
		SwipeActions.swipeDownwards(driver, "up");
	}
	
	public void click_On_Logout()
	{
		logout.navigateToLogout.click();
	}

}
