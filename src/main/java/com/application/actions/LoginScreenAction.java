package com.application.actions;

import com.application.base.RunnableClass;
import com.application.pages.LoginScreen;

public class LoginScreenAction extends RunnableClass {

	LoginScreen ls = new LoginScreen(driver);

	public void language_Select() throws InterruptedException {
		// ls.navigateToLanguage.click();
		// ls.navigateToContinueLanguage.click();
		Thread.sleep(3000);
		ls.navigateToLoginButton.click();
	}

	public void username_Field(String username) {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ls.navigateToUsernameField.sendKeys(username);
//		driver.hideKeyboard();
		ls.navigateToContinue.click();
	}

	public void password_Field(String password) {
		ls.navigateToPasswordField.sendKeys(password);
//		driver.hideKeyboard();
	}

	public void click_On_ContinueButton() throws InterruptedException {
		ls.navigateToContinue.click();
		ls.navigateToContinue.click();
		Thread.sleep(3000);
		ls.navigateToLater.click();
	}
}
