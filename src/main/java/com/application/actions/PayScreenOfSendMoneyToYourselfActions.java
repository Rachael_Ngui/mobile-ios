package com.application.actions;

import com.application.base.RunnableClass;
import com.application.pages.PayScreenOfSendMoneyToYourself;

public class PayScreenOfSendMoneyToYourselfActions extends RunnableClass {

	PayScreenOfSendMoneyToYourself ps = new PayScreenOfSendMoneyToYourself(driver);

	public void transact() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		ps.navigateToTransact.click();
	}

	public void sendMoneyToYourself() throws InterruptedException {
		ps.navigateToSendMoneyToYourself.click();
		//ps.navigateToChooseAccount.click();
		//ps.navigateToAccountRadioSelect.click();
	}

	public void amount(String amount) {
		ps.navigateToAmountTextField.sendKeys(amount);
	}

	public void sendMoneyButton() {
		ps.navigateToSendMoneyButton.click();
	}

	public void Pay() {
		ps.navigateToPayButton.click();
	}

	public void Done() {
		ps.navigateToDoneButton.click();
	}
}
