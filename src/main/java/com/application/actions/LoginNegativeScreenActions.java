package com.application.actions;

import com.application.base.RunnableClass;
import com.application.pages.LoginNegativeScreen;

public class LoginNegativeScreenActions extends RunnableClass{

	LoginNegativeScreen ls=new LoginNegativeScreen(driver);
	
	public void Continue()
	{
		//driver.hideKeyboard();
		ls.navigateToContinue.click();
	}
}
