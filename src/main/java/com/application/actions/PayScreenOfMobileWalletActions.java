package com.application.actions;

import com.application.base.RunnableClass;
import com.application.pages.PayScreenOfMobileWallet;

public class PayScreenOfMobileWalletActions extends RunnableClass {

	PayScreenOfMobileWallet psm = new PayScreenOfMobileWallet(driver);

	public void sendMoneyToMobileWallet() {
		psm.navigateToTransact.click();
		psm.navigateToSendMoneyToSomeOne.click();
	}

	public void mobile_wallet() {
		psm.navigateToMobileWallet.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void select_Account() {
		psm.navigateToSelectAccount.click();
	}

	public void someOne_MobileWallet() {
		psm.navigateToSomeOnenew.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void operator() {
		try {
			psm.navigateToOperatorOfSudan.click();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			psm.navigateToOperaatorOfTanzania.click();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			psm.navigateToOpeartorOfRwandaMTN.click();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			psm.navigateToOpeartorOfUgandaMTN.click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	public void selectCountry() {
//		psm.navigateToSelectCountry.click();
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//
//	public void country() {
//		psm.navigateToKenya.click();
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//
//	public void MPESA() {
//		psm.navigateToMPESA.click();
//	}

	public void number(String number) {
		psm.navigateToNumberField.sendKeys(number);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void add() {
		psm.navigateToAdd.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void mobile_Wallet_amount(String amount) {
		psm.navigateToMAobileWalletmountTextField.sendKeys(amount);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void send_Money() {
		psm.navigateToSendMoneyButton.click();
	}

	public void Pay() {
		psm.navigateToPayButton.click();
	}

	public void Done() {
		psm.navigateToDoneButton.click();
		driver.navigate().back();
		driver.navigate().back();
	}
}
