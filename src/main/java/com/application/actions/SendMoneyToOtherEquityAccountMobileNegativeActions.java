package com.application.actions;

import com.application.base.RunnableClass;
import com.application.pages.SendMoneyToOtherEquityAccountMobileNegative;

public class SendMoneyToOtherEquityAccountMobileNegativeActions extends RunnableClass{

	SendMoneyToOtherEquityAccountMobileNegative s =new SendMoneyToOtherEquityAccountMobileNegative(driver);
	
	public void sendMoneyToOtherEquity() {
		s.navigateToTransact.click();
		s.navigateToSendMoneyToSomeone.click();
		s.navigateToEquityAccount.click();
	}

	public void select_Account() {
		s.navigateToSelectAccount.click();
	}

	public void someOne() {
		s.navigateToSomeOnenew.click();
	}

	public void mobile_number_field(String number) {
		s.navigateToOtherEquityAccountNumberField.sendKeys(number);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void add() {
		s.navigateToAdd.click();
		driver.navigate().back();
		driver.navigate().back();
		driver.navigate().back();
		driver.navigate().back();
	}

}
