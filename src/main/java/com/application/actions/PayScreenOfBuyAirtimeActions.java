package com.application.actions;

import com.application.base.RunnableClass;
import com.application.libraries.SwipeActions;
import com.application.pages.PayScreenOfBuyAirtime;

public class PayScreenOfBuyAirtimeActions extends RunnableClass {

	PayScreenOfBuyAirtime psb = new PayScreenOfBuyAirtime(driver);

	public void buyAirTime() {
//		driver.navigate().back();
		psb.navigateToTransact.click();
		SwipeActions.swipeDownwards(driver, "up");
		psb.navigateToBuyAirtime.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void select_Account() {
		psb.navigateToSelectAccount.click();
	}

	public void someOneNew() {
		psb.navigateToSomeOnenew.click();
	}

	public void RadioButton() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		psb.navigateToMTN.click();
	}

	public void number(String number) {
		psb.navigateToBuyAirtimeNumberField.sendKeys(number);
	}

	public void add() {
		psb.navigateToAdd.click();
	}

	public void amount(String amount) {
		psb.navigateToBuyAirtimeTextField.sendKeys(amount);
	}

	public void buy_airtime() {
		psb.navigateToBuyAirtimeButton.click();
		psb.navigateToBuyAirtime.click();
	}

	public void done() {
		psb.navigateToDoneButton.click();
	}
}
