package com.application.actions;

import com.application.base.RunnableClass;
import com.application.pages.SendMoneyToOtherEquityAccountAmountNegative;

public class SendMoneyToOtherEquityAccountAmountNegativeActions extends RunnableClass {

	SendMoneyToOtherEquityAccountAmountNegative s = new SendMoneyToOtherEquityAccountAmountNegative(driver);

	public void sendMoneyToOtherEquity() {
		s.navigateToTransact.click();
		s.navigateToSendMoneyToSomeone.click();
		s.navigateToEquityAccount.click();
	}

	public void select_Account() {
		s.navigateToSelectAccount.click();
	}

	public void someOne() {
		s.navigateToSomeOnenew.click();
	}

	public void mobile_number_field(String number) {
		s.navigateToOtherEquityAccountNumberField.sendKeys(number);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void add() {
		s.navigateToAdd.click();
	}

	public void account_field(String amount) {
		s.navigateToOtherEqyityAmountTextField.sendKeys(amount);
		driver.navigate().back();
		driver.navigate().back();
	}

}
