package com.application.actions;

import com.application.base.RunnableClass;
import com.application.pages.SendMoneyToMobileWalletNegative;

public class SendMoneyToMobileWalletNegativeActions extends RunnableClass{

	SendMoneyToMobileWalletNegative s = new SendMoneyToMobileWalletNegative(driver);
	
	public void sendMoneyToMobileWallet() {
		s.navigateToTransact.click();
		s.navigateToSendMoneyToSomeOne.click();
	}
	
	public void mobile_wallet()
	{
		s.navigateToMobileWallet.click();
	}
	
	public void select_Account()
	{
		s.navigateToSelectAccount.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void someOne_MobileWallet()
	{
		s.navigateToSomeOnenew.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void selectCountry()
	{
		s.navigateToSelectCountry.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void country()
	{
		s.navigateToKenya.click();
	}
	
	public void MPESA()
	{
		s.navigateToMPESA.click();
	}
	
	public void number(String number)
	{
		s.navigateToNumberField.sendKeys(number);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.navigate().back();
		driver.navigate().back();
		driver.navigate().back();
		driver.navigate().back();
		driver.navigate().back();
	}
}
