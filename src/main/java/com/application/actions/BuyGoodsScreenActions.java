package com.application.actions;

import com.application.base.RunnableClass;
import com.application.pages.BuyGoodsScreen;

public class BuyGoodsScreenActions extends RunnableClass {

	BuyGoodsScreen b = new BuyGoodsScreen(driver);

	public void buygoods() {
		b.navigateToTransact.click();
		b.navigateToBuyGoods.click();
	}

	public void select_account() {
		b.navigateToSelectAccount.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void pay_another_till() {
		b.navigateToTill.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void country() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		b.navigateToCountry.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		b.navigateToRwanda.click();
	}

	public void select_till() {
		b.navigateToSelectTill.click();
		b.navigateToAirtelMoney.click();
	}

	public void account_number(String number) {
		b.naviagteToAccountNumber.sendKeys(number);
	}

	public void add() {
		b.navigateToAdd.click();
	}

	public void bill_amount(String amount) {
		b.navigateToPayBillAmount.sendKeys(amount);
	}

	public void Continue() {
		b.navigateToContinueButton.click();
	}

	public void pay() {
		b.navigateToPayButton.click();
	}

	public void done() {
		b.navigateToDoneButton.click();
	}

}
