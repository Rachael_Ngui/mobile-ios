package com.application.actions;

import com.application.base.RunnableClass;
import com.application.pages.SendMoneyToOtherEquityAccountUsingAccountNumber;

public class PayScreenOfOtherEquityAccountMobileActions extends RunnableClass {

	SendMoneyToOtherEquityAccountUsingAccountNumber m = new SendMoneyToOtherEquityAccountUsingAccountNumber(driver);

	public void sendMoneyToOtherEquity() {
		m.navigateToTransact.click();
		m.navigateToSendMoneyToSomeOne.click();
		m.navigateEquityAccount.click();
	}

	public void select_Account() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		m.navigateToSelectAccount.click();
	}

	public void someOne() {
		m.navigateToSomeOnenew.click();
	}

	public void account_number_field(String account) {
		m.navigateToOtherEquityMobileNumberField.sendKeys(account);
	}

	public void add() {
		m.navigateToAdd.click();
	}

	public void account_field(String amount) {
		m.navigateToOtherEqyityAmountTextField.sendKeys(amount);
	}

	public void send() {
		m.navigateToSendMoneyButton.click();
	}

	public void pay() {
		m.navigateToPayButton.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void done() {
		m.navigateToDoneButton.click();
		driver.navigate().back();
		driver.navigate().back();
	}
}
