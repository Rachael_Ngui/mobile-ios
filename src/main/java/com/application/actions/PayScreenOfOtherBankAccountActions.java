package com.application.actions;

import com.application.base.RunnableClass;
import com.application.pages.PayScreenOfOtherBankAccount;

public class PayScreenOfOtherBankAccountActions extends RunnableClass {

	PayScreenOfOtherBankAccount p = new PayScreenOfOtherBankAccount(driver);

	public void OtherBankAccount() {
		p.navigateToTransact.click();
		p.navigateToSendMoneyToSomeOne.click();
		p.navigateToOtherBankAccount.click();
	}

	public void select_Account() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		p.navigateToSelectAccount.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void SomeOneNew() {
		p.navigateToSomeOnenew.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void search(String Bankname) {
		p.navigateToSearch.sendKeys(Bankname);
//		p.navigateToECO.click();
		p.navigateToExim.click();
	}

	public void fullName(String name) {
		p.navigateToFullName.sendKeys(name);
	}

	public void accountNumber(String number) {
		p.navigateToAccountNumberField.sendKeys(number);
	}

//	public void number(String number1) {
//		p.navigateToNumberField.sendKeys(number1);
//	}

	public void add() {
		p.navigateToAdd.click();
	}

	public void amountField(String amount) {
		p.navigateToAmountTextField.sendKeys(amount);
	}

	public void send() {
		p.navigateToSendMoneyButton.click();
	}

	public void pay() {
		p.navigateToPayButton.click();
	}

	public void done() {
		p.navigateToDoneButton.click();
	}
}
