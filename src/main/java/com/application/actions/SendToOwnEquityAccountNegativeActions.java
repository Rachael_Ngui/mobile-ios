package com.application.actions;

import com.application.base.RunnableClass;
import com.application.pages.SendToOwnEquityAccountNegative;

public class SendToOwnEquityAccountNegativeActions extends RunnableClass {

	SendToOwnEquityAccountNegative s = new SendToOwnEquityAccountNegative(driver);

	public void transact() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		s.navigateToTransact.click();
	}

	public void sendMoneyToYourself() throws InterruptedException {
		s.navigateToSendMoneyToYourself.click();
	}

	public void amount(String amount) {
		s.navigateToAmountTextField.sendKeys(amount);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.navigate().back();
	}
}
