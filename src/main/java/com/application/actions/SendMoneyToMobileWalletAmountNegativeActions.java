package com.application.actions;

import com.application.base.RunnableClass;
import com.application.pages.SendMoneyToMobileWalletAmoutNegative;

public class SendMoneyToMobileWalletAmountNegativeActions extends RunnableClass{

	SendMoneyToMobileWalletAmoutNegative s = new SendMoneyToMobileWalletAmoutNegative(driver);
	
	public void sendMoneyToMobileWallet() {
		s.navigateToTransact.click();
		s.navigateToSendMoneyToSomeOne.click();
	}
	
	public void mobile_wallet()
	{
		s.navigateToMobileWallet.click();
	}
	
	public void select_Account()
	{
		s.navigateToSelectAccount.click();
	}
	
	public void someOne_MobileWallet()
	{
		s.navigateToSomeOnenew.click();
	}
	
	public void selectCountry()
	{
		s.navigateToSelectCountry.click();
	}
	
	public void country()
	{
		s.navigateToKenya.click();
	}
	
	public void MPESA()
	{
		s.navigateToMPESA.click();
	}
	
	public void number(String number)
	{
		s.navigateToNumberField.sendKeys(number);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void add()
	{
		s.navigateToAdd.click();
	}
	
	public void mobile_Wallet_amount(String amount)
	{
		s.navigateToMAobileWalletmountTextField.sendKeys(amount);
		driver.navigate().back();
		driver.navigate().back();
	}
}
