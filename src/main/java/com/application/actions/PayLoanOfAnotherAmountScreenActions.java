package com.application.actions;

import com.application.base.RunnableClass;
import com.application.pages.PayLoanOfAnotherAmountScreen;

public class PayLoanOfAnotherAmountScreenActions extends RunnableClass{

	PayLoanOfAnotherAmountScreen p=new PayLoanOfAnotherAmountScreen(driver);
	
	public void payLoan()
	{
		p.navigteToPayLoan.click();
	}
	
	public void selectLoanPay()
	{
		p.navigateToSelectLoan.click();
	}
	
	public void option()
	{
		p.navigateToFirst.click();
	}
	
	public void amount()
	{
		p.navigateToChoose.click();
	}
	
	public void anotherAmount()
	{
		p.navigateToAnotherAmount.click();
	}
	
	public void anotherAmountEnter(String amount)
	{
		p.navigateToAmountField.sendKeys(amount);
	}
	
	public void save()
	{
		p.navigateToSave.click();
	}
	
	public void Continue()
	{
		p.navigateToContinue.click();
	}
	
	public void payment()
	{
		p.navigateToMakePayment.click();
	}
	
}
