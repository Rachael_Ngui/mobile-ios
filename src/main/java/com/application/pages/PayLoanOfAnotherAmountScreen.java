package com.application.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

public class PayLoanOfAnotherAmountScreen {

	public PayLoanOfAnotherAmountScreen(IOSDriver<WebElement> driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name = "Pay loan")
	public WebElement navigteToPayLoan;
	
	@FindBy(name = "Select loan to pay")
	public WebElement navigateToSelectLoan;
	
	@FindBy(xpath= "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]")
	public WebElement navigateToFirst;
	
	@FindBy(name = "Choose amount to pay")
	public WebElement navigateToChoose;
	
	@FindBy(name = "Another amount")
	public WebElement navigateToAnotherAmount;
	
	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[4]/XCUIElementTypeTextField")
	public WebElement navigateToAmountField;
	
	@FindBy(name = "Save")
	public WebElement navigateToSave;
	
	@FindBy(name = "Continue")
	public WebElement navigateToContinue;
	
	@FindBy(name = "Make payment")
	public WebElement navigateToMakePayment;
}
