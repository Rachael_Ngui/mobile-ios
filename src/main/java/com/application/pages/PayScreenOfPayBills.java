package com.application.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

public class PayScreenOfPayBills {

	public PayScreenOfPayBills(IOSDriver<WebElement> driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name = "Transact")
	public WebElement navigateToTransact;
	
	@FindBy(name = "Pay bill")
	public WebElement navigateToPayBills;

	@FindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Select biller\"]")
	public WebElement navigateToSelectBiller;

	@FindBy(name = "Utilities")
	public WebElement navigateToUtilities;

	//@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeTextField")
	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeTextField")
	public WebElement navigateToSearch;

//	@FindBy(name = "RIZIKI INSURANCE")
//	public WebElement navigateToRizikiInsurance;
	
	//300101 for Tanzania
	@FindBy(name = "SELCOM DAWASCO")
	public WebElement navigateTo300101;
	
	
	
	
	
	@FindBy(name = "Next")
	public WebElement navigateToNextButton;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeTextField")
	public WebElement navigateToPayBillAccountnumber;
	
	@FindBy(name = "Add")
	public WebElement navigateToAdd;

	//@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[5]/XCUIElementTypeTextField")
	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[5]/XCUIElementTypeTextField")
	public WebElement navigateToPayBillAmount;
	
	@FindBy(name = "Continue")
	public WebElement navigateToContinueButton;

	@FindBy(name = "Pay")
	public WebElement navigateToPayButton;

	@FindBy(name = "Done")
	public WebElement navigateToDoneButton;
}
