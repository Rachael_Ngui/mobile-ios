package com.application.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

public class PayScreenOfBuyAirtime {

	public PayScreenOfBuyAirtime(IOSDriver<WebElement> driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name = "Transact")
	public WebElement navigateToTransact;
	
	@FindBy(name = "Buy airtime")
	public WebElement navigateToBuyAirtime;
	
	@FindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Select Account\"]")
	public WebElement navigateToSelectAccount;
	
	@FindBy(name = "Send to someone new")
	public WebElement navigateToSomeOnenew;

//	@FindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Equitel\"]")
//	public WebElement navigateToEquitelRadioButton;
//
//	@FindBy(xpath = "//XCUIElementTypeStaticText[@name=\"M-PESA\"]")
//	public WebElement navigateToMPESARadioButton;
	
	
	@FindBy(name = "MTN")
	public WebElement navigateToMTN;
	
	@FindBy(name = "Tigo")
	public WebElement navigateToTIGO;
	
	@FindBy(name = "Airtel")
	public WebElement navigateToAirtel;
	
	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeTextField")
	public WebElement navigateToBuyAirtimeNumberField;
	
	@FindBy(name = "Add")
	public WebElement navigateToAdd;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[6]/XCUIElementTypeTextField")
	public WebElement navigateToBuyAirtimeTextField;
	
	@FindBy(xpath= "//XCUIElementTypeButton[@name=\"Buy airtime\"]")
	public WebElement navigateToBuyAirtimeButton;

	@FindBy(name = "Done")
	public WebElement navigateToDoneButton;
}
