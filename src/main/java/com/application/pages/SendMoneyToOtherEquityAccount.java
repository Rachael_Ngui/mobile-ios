package com.application.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

public class SendMoneyToOtherEquityAccount {

	public SendMoneyToOtherEquityAccount(IOSDriver<WebElement> driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(name = "Transact")
	public WebElement navigateToTransact;

	@FindBy(name = "Send money to someone")
	public WebElement navigateToSendMoneyToSomeOne;
	
	@FindBy(name = "Equity account")
	public WebElement navigateEquityAccount;

	@FindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Select Account\"]")
	public WebElement navigateToSelectAccount;

	@FindBy(name = "Send to someone new")
	public WebElement navigateToSomeOnenew;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeTextField")
	public WebElement navigateToOtherEquityAccountNumberField;

	@FindBy(name = "Add")
	public WebElement navigateToAdd;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[6]/XCUIElementTypeTextField")
	public WebElement navigateToOtherEqyityAmountTextField;

	@FindBy(name = "Send money")
	public WebElement navigateToSendMoneyButton;

	@FindBy(name = "Pay")
	public WebElement navigateToPayButton;

	@FindBy(name = "Done")
	public WebElement navigateToDoneButton;
}
