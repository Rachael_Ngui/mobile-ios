package com.application.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

public class LoginScreen {

	public LoginScreen(IOSDriver<WebElement> driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(name = "English")
	public WebElement navigateToLanguage;

	@FindBy(name = "Anza")
	public WebElement navigateToContinueLanguage;

	@FindBy(name = "Sign in")
	public WebElement navigateToLoginButton;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[6]/XCUIElementTypeTextField")
	public WebElement navigateToUsernameField;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[10]/XCUIElementTypeSecureTextField")
	public WebElement navigateToPasswordField;

	@FindBy(name = "Continue")
	public WebElement navigateToContinue;

	@FindBy(name = "Ok, set up now")
	public WebElement navigateToLater;
}
