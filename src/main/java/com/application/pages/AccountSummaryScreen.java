package com.application.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

public class AccountSummaryScreen {

	public AccountSummaryScreen(IOSDriver<WebElement> driver) {
		PageFactory.initElements(driver, this);
	}

//	@FindBy(name = "John Mathiang Alou")
	@FindBy(name = "14941900.0 TZS")
	public WebElement navigateToAccountListAndClick;
	
	@FindBy(name = "85,722.44 SSP")
	public WebElement navigateToLuate;

	//Rwanda
	@FindBy(name = "873,217.12 RWF") // Mugume Alex
	public WebElement navigateToMugumeALex;
	
	//Uganda
	@FindBy(name = "583,375.31 UGX")
	public WebElement navigateToMuyanja; //Muyanja hassan

	@FindBy(name = "Help")
	public WebElement navigateToBalance;

	@FindBy(name = "Search & filter")
	public WebElement navigateToSearchFilter;

	@FindBy(name = "Filter")
	public WebElement navigateToFilter;

	@FindBy(name = "1 Month")
	public WebElement navigateTo1Month;

	@FindBy(name = "3 Months")
	public WebElement navigateTo3Month;

	@FindBy(name = "6 Months")
	public WebElement navigateTo6Month;

	@FindBy(name = "1 Year")
	public WebElement navigateTo1Year;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[3]/XCUIElementTypeTextField")
	public WebElement navigateToFromDate;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[5]/XCUIElementTypeTextField")
	public WebElement navigateToToDate;

	@FindBy(name = "Money in")
	public WebElement navigateToMoneyIn;

	@FindBy(name = "Money out")
	public WebElement navigateToMoneyOut;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[8]/XCUIElementTypeTextField[2]")
	public WebElement navigateToMinTextField;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[8]/XCUIElementTypeTextField[1]")
	public WebElement navigateToMaxTextField;

	@FindBy(name = "Clear filter")
	public WebElement navigateToClear;

	@FindBy(name = "Apply filter")
	public WebElement navigateToApplyFilter;

}
