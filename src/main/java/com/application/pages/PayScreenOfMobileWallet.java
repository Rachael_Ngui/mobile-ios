package com.application.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

public class PayScreenOfMobileWallet {

	public PayScreenOfMobileWallet(IOSDriver<WebElement> driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(name = "Transact")
	public WebElement navigateToTransact;

	@FindBy(name = "Send money to someone")
	public WebElement navigateToSendMoneyToSomeOne;

	@FindBy(name = "Mobile wallet")
	public WebElement navigateToMobileWallet;

	@FindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Select Account\"]")
	public WebElement navigateToSelectAccount;

	@FindBy(name = "Send to someone new")
	public WebElement navigateToSomeOnenew;

	// Operator name for South Sudan account
	@FindBy(name = "Mgurush")
	public WebElement navigateToOperatorOfSudan;

	@FindBy(name = "Select Country")
	public WebElement navigateToSelectCountry;

	@FindBy(name = "Kenya")
	public WebElement navigateToKenya;

	@FindBy(name = "MPesa")
	public WebElement navigateToMPESA;

	// Operator name for Tanzania
	@FindBy(name = "VODACOM")
	public WebElement navigateToOperaatorOfTanzania;
	
	// Operator name for Rwanda
	@FindBy(name = "MTN")
	public WebElement navigateToOpeartorOfRwandaMTN;
	
	@FindBy(name = "TIGO")
	public WebElement navigateToOpeartorOfRwandaTIGO;
	
	//Opeartor name for Uganda
	@FindBy(name = "MTN")
	public WebElement navigateToOpeartorOfUgandaMTN;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeTextField")
	public WebElement navigateToNumberField;

	@FindBy(name = "Add")
	public WebElement navigateToAdd;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[6]/XCUIElementTypeTextField")
	public WebElement navigateToMAobileWalletmountTextField;

	@FindBy(name = "Send money")
	public WebElement navigateToSendMoneyButton;

	@FindBy(name = "Pay")
	public WebElement navigateToPayButton;

	@FindBy(name = "Done")
	public WebElement navigateToDoneButton;

}
