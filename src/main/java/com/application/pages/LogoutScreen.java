package com.application.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

public class LogoutScreen {

	public LogoutScreen(IOSDriver<WebElement> driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(name = "More")
	public WebElement navigateToMore;

	@FindBy(name = "Log out")
	public WebElement navigateToLogout;

}
