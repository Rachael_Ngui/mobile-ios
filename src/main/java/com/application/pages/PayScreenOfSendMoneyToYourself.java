package com.application.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

public class PayScreenOfSendMoneyToYourself {

	public PayScreenOfSendMoneyToYourself(IOSDriver<WebElement> driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(name = "Transact")
	public WebElement navigateToTransact;

	@FindBy(name = "Send money to your own account")
	public WebElement navigateToSendMoneyToYourself;
	
	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]")
	public WebElement navigateToChooseAccount;
	
//	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[4]")
	@FindBy(name  = "MAGRETH,MWAMVUA AND DOR...")
	public WebElement navigateToAccountRadioSelect;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[6]/XCUIElementTypeTextField")
	public WebElement navigateToAmountTextField;

	@FindBy(name = "Send money")
	public WebElement navigateToSendMoneyButton;

	@FindBy(name = "Pay")
	public WebElement navigateToPayButton;

	@FindBy(name = "Done")
	public WebElement navigateToDoneButton;

}
