package com.application.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

public class SendMoneyToMobileWalletNegative {

	public SendMoneyToMobileWalletNegative(IOSDriver<WebElement> driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name = "Transact")
	public WebElement navigateToTransact;
	
	@FindBy(name = "Send money to someone")
	public WebElement navigateToSendMoneyToSomeOne;

	@FindBy(name = "Mobile wallet")
	public WebElement navigateToMobileWallet;

	@FindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Select Account\"]")
	public WebElement navigateToSelectAccount;

	@FindBy(name = "Someone new")
	public WebElement navigateToSomeOnenew;
	
	@FindBy(name = "Select Country")
	public WebElement navigateToSelectCountry;
	
	@FindBy(name = "Kenya")
	public WebElement navigateToKenya;
	
	@FindBy(name = "MPesa")
	public WebElement navigateToMPESA;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeTextField")
	public WebElement navigateToNumberField;
}
