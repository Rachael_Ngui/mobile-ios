package com.application.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

public class PayScreenOfOtherBankAccount {

	public PayScreenOfOtherBankAccount(IOSDriver<WebElement> driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name = "Transact")
	public WebElement navigateToTransact;
	
	@FindBy(name = "Send money to someone")
	public WebElement navigateToSendMoneyToSomeOne;
	
	@FindBy(name = "Another bank account")
	public WebElement navigateToOtherBankAccount;
	
	@FindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Select Account\"]")
	public WebElement navigateToSelectAccount;
	
	@FindBy(name = "Send to someone new")
	public WebElement navigateToSomeOnenew;
	
	@FindBy(name = "South Sudan")
	public WebElement navigateToSouthSudan;
	
	//@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[3]/XCUIElementTypeTextField")
	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]/XCUIElementTypeTextField")
	public WebElement navigateToSearch;
	
//	@FindBy(name = "ECO BANK")
//	public WebElement navigateToECO;
	
	@FindBy(name = "National Microfinance Bank")
	public WebElement navigateToNational;
	
	@FindBy(name = "EXIM BANK TZ LTD")
	public WebElement navigateToExim;
	
	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeTextField")
	public WebElement navigateToFullName;
	
	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[4]/XCUIElementTypeTextField")
	public WebElement navigateToAccountNumberField;
	
	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[6]/XCUIElementTypeTextField")
	public WebElement navigateToNumberField;
	
	@FindBy(name = "Add")
	public WebElement navigateToAdd;
	
	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"Equity\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[6]/XCUIElementTypeTextField")
	public WebElement navigateToAmountTextField;
	
	@FindBy(name = "Send money")
	public WebElement navigateToSendMoneyButton;

	@FindBy(name = "Pay")
	public WebElement navigateToPayButton;

	@FindBy(name = "Done")
	public WebElement navigateToDoneButton;
}
