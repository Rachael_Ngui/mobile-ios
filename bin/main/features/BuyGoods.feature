#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@UATBuyGoods
Feature: Buy Goods
  I want to use this template for my feature file

		Scenario: Login to Equity
    Given user launch Equity app
    When user logs in using username
    And password
 		Then Home screen should appear 
 		
  @tag1
  Scenario: Checking for Buy Goods
    Given user naviate to Buy goods
    And select account
    When user pay through another till
    And user selects country
    And user selects till
    Then user enters account number of Buy goods
    And clicks on add
    And user enters bill amount 
    Then user clicks on Continue of Buy goods
    And user clicks on pay of Buy goods
    And user clicks on done of Buy goods
    
Scenario: Logout from Equity
    When navigate to More in menu option
    Then locate the logout option by scrolling down
    And logout from application by clicking
    