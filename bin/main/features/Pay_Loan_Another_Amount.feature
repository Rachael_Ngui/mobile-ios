#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: Loan feature

@positiveScenario
Scenario: Test the Pay Loan feature
Given navigate to Pay Loan
And the select the loan source to pay
And choose the first option
Then choose the amount to pay
Then choose amother amount
And enter the amount to pay "122"
And click on save button
Then click on continue button
And finaly make the payment
