#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: Login Functionality Feature with Negative usecases

In order to ensure Login Functionality works by passing invalid credentials,
I want to run the cucumber test to verify it is working

 		@negativeScenario
 		Scenario Outline: Login fail - possible combinations
 		Given user launch Equity app
 		When user logs in using username as "<Username>"
 		And password as "<Password>"
 		And user clicks Continue button
    Then user gets login failed error message
 		
 		Examples:
 		|Username			|Password			|
 		|254796950470	|Test123!			|
 		|25479695047	|Pass123!!!		|
 		|222222222222	|Pass1!				|
 		