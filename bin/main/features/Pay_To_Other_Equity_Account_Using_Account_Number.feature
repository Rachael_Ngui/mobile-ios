#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@UATPayToOtherEquityAccountUsingAccountNumber
Feature: Pay Functionality feature
  I want to use this template for Pay Transactions

		@positiveScenario
		Scenario: Login to Equity
    Given user launch Equity app
    When user logs in using username
    And password
 		Then Home screen should appear  
 		
  	@positiveScenario 
  	Scenario: Checking various Transactions using account number for Equity Account
  	Then click on send money to Other Equity Account using Account number
  	And click on select account using Account number
  	Then user select the some one using Account number
  	Then user enters the account number using Account number
  	And click on Add button in Other Equity Account using Account number
  	Then user enters other Equity amount using Account number
  	And click on send money button of Other Equity Account using Account number
  	Then click on Pay button of Other Equity Account using Account number
  	And click on Done button of Other Equity Account using Account number
  	
  	@postivieScenario
  	Scenario: Logout from Equity
    When navigate to More in menu option
    Then locate the logout option by scrolling down
    And logout from application by clicking