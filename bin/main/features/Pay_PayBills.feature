#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@UATPayBills
Feature: Pay Functionality feature
  I want to use this template for Pay Transactions
  
    Scenario: Login to Equity
    Given user launch Equity app
    When user logs in using username
    And password
 		Then Home screen should appear 

  @positiveScenario 
  	Scenario: Checking various Transactions using PayBills
  	Then user navigate to PayBills
  	And user clicks on selectBiller
  	Then user select utilities
  	And user searches for
  #	Then user clicks on Next button
  	And user clicks on account number
  	Then user clicks on add button
  	And user enters amounts
  	And user clicks on continue
  	And user clicks on pay
  	Then user clicks on Done
  	
  	Scenario: Logout from Equity
    When navigate to More in menu option
    Then locate the logout option by scrolling down
    And logout from application by clicking
  	
