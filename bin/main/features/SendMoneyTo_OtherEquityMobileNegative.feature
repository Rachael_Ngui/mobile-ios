#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: Pay Functionality feature
  I want to use this template for Pay Transactions
  
    @positiveScenario
		Scenario: Login to Equity
    Given user launch Equity app
    When user logs in using username as "254763346690"
    And password as "Pass123!"
 		Then Home screen should appear 

  @negativeScenario 
  	Scenario Outline: Checking various Transactions in mobile number for Equity Account
  	Then click on send money to Other Equity Account in mobile number
  	And click on select account in mobile number
  	Then user select the some one in mobile number
  	Then user enters the account number "<Mobile>" in mobile number
  	And click on add
  	
  	Examples:
  	  | Mobile  					| 
      | 123			 					|  
      | 889988998877799 	|
      |	666666asd					|
      |174086.06					| 
      
   @postivieScenario
  	Scenario: Logout from Equity
    When navigate to More in menu option
    Then locate the logout option by scrolling down
    And logout from application by clicking
