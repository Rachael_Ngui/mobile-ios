#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@UATPayToOtherEquityAccountUsingMobileNumber
Feature: Pay Functionality feature
  I want to use this template for Pay Transactions
 		
 		Scenario: Login to Equity
    Given user launch Equity app
    When user logs in using username as "254763950055"
    And password as "Ss@12345"
 		Then Home screen should appear 
 		
  @positiveScenario
  Scenario: Checking various Transactions using mobile number of Equity Account
  	Then click on send money to Other Equity Account
  	And click on select account
  	Then user select the some one
  	Then user enters the mobile number "0765555078"
  	And click on Add button in Other Equity Account
  	Then user enters other Equity amount "101"
  	And click on send money button of Other Equity Account
  	Then click on Pay button of Other Equity Account
  	And click on Done button of Other Equity Account
